from fastapi.testclient import TestClient
from main import app
from postgres.db import CategoryQueries

client = TestClient(app)

class EmptyCategoryQueries:
    def get_category(self, id):
        return None

class NormalCategoryQueries:
    def get_category(self, id):
        return [id, "OUR CATEGORY", True]

    def get_all_categories(self, page: int):
        return 3001, [[1,"TACO", True, 42068], [2, "QUESO", False, 0]]
    
    def insert_category(self, title):
        return [5, "SANDWICH", True]


def test_get_category_returns_404():
    # ARRANGE
    # Use our fake database
    app.dependency_overrides[CategoryQueries] = EmptyCategoryQueries

    # ACT
    # Make the request
    response = client.get("/api/postgres/categories/1")

    # ASSERT
    # Assert that we got a 404
    assert response.status_code == 404

    # CLEAN UP
    # Clear out the dependencies
    app.dependency_overrides = {}




def test_get_category_returns_200():
    # ARRANGE
    app.dependency_overrides[CategoryQueries] = NormalCategoryQueries

    # ACT
    response = client.get("/api/postgres/categories/1")
    d = response.json()

    # ASSERT
    assert response.status_code == 200
    assert d["id"] == 1
    assert d["title"] == "OUR CATEGORY"
    assert d["canon"] == True

    # CLEAN UP
    app.dependency_overrides = {}


def test_get_category_returns_list():
    # ARRANGE
    app.dependency_overrides[CategoryQueries] = NormalCategoryQueries

    # ACT
    response = client.get("/api/postgres/categories/")
    d = response.json()

    # ASSERT
    assert response.status_code == 200
    assert d["page_count"] == 3001
    assert len(d["categories"]) == 2
    assert d["categories"][0]["id"] == 1

    # CLEAN UP
    app.dependency_overrides = {}


def test_insert_category_returns_200():
    app.dependency_overrides[CategoryQueries] = NormalCategoryQueries    

    response = client.post("/api/postgres/categories",
        headers={"Content-Type": "application/json"},
        json={"id": 5, "title": "SANDWICH", "canon": True},
    )
    assert response.status_code == 200
    assert response.json() == {"id": 5, "title": "SANDWICH", "canon": True}

    app.dependency_overrides = {}
